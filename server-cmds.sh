#!/usr/bin/env bash

export IMAGE="$1:$2"
docker-compose -f docker-compose.yml up -d
echo "success ${IMAGE}"