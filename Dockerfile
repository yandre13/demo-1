FROM node:14-alpine

WORKDIR /src/jenkins/app

COPY index.js .

CMD [ "node index.js" ]